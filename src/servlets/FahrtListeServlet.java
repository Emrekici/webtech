// Ersteller: Timo Iwan und Emre Ekici
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import beans.FahrtListeBean;


@WebServlet("/FahrtListeServlet")
public class FahrtListeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
       
    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource ds;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        
        final HttpSession session = request.getSession();
        List<FahrtListeBean> fahrten = new ArrayList<>();
        
        if(session.getAttribute("gefundeneFahrten") != null) {
            fahrten = (List<FahrtListeBean>) session.getAttribute("gefundeneFahrten");
            request.setAttribute("gefundeneFahrten", null);
        } else {
            String kategorie = request.getParameter("kategorie");
            fahrten= search(kategorie);

        }

                request.setAttribute("fahrten", fahrten);
                session.setAttribute("fahrten", fahrten);

                response.sendRedirect("jsp/hauptseite.jsp");
               
    }
    

    private List<FahrtListeBean> search(String iD) throws ServletException {
        SimpleDateFormat format2 = new SimpleDateFormat("dd.MM.yyyy");
     
        List<FahrtListeBean> fahrten = new ArrayList<FahrtListeBean>();
        
        try (Connection con = ds.getConnection();
             PreparedStatement pstmt = con.prepareStatement("SELECT * FROM fahrten ")) {
            
        
            try (ResultSet rs = pstmt.executeQuery()) {
            
                while (rs.next()) {
                    FahrtListeBean fahrtliste = new FahrtListeBean();
                    
                    Long id1 = rs.getLong("id");
                    fahrtliste.setId(id1);
                    
                    String name = rs.getString("name");
                    fahrtliste.setName(name);
                    
                    String kategorie = rs.getString("kategorie");
                    fahrtliste.setKategorie(kategorie);
                    
                    String zielgebiet = rs.getString("zielgebiet");
                    fahrtliste.setZielgebiet(zielgebiet);
                    
                    String aDatum = format2.format(rs.getDate("datum"));
                    fahrtliste.setaDatum(aDatum);
                    
                    Date datum = rs.getDate("datum");
                    fahrtliste.setDatum(datum);
                    
                    String seats = rs.getString("seats");
                    fahrtliste.setSeats(seats);
                    
                
                    fahrten.add(fahrtliste);
                } 
            }
        } catch (Exception ex) {
            throw new ServletException(ex.getMessage());
        }
        
        return fahrten;
    }
    

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doPost(request, response);
    
    }

    

}
