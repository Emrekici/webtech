// Ersteller: Timo Iwan

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import javax.sql.DataSource;
import beans.BildBlobBean;
/**
 * Servlet implementation class NeueFahrtServlet
 */
@WebServlet("/BildBlobServlet")
@MultipartConfig(location="/tmp")
		
public class BildBlobServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource ds;
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 request.setCharacterEncoding("UTF-8");
		BildBlobBean bild = new BildBlobBean();
		bild.setInfo(request.getParameter("info"));

		
		
		//FILE
			Part filepart = request.getPart("bild");
			bild.setBildName(filepart.getName());

			//DB-Zugriff
			persist(bild, filepart);
		//SESSION
				HttpSession session = request.getSession();
				session.setAttribute("bild", bild);
				
		response.sendRedirect("jsp/profileinstellungen.jsp?status=success");
//		
//				RequestDispatcher rd = request.getRequestDispatcher("jsp/neueFahrtAusgabe.jsp");
//				rd.forward(request, response);
		
				
}
 
 
 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");;
		response.setCharacterEncoding("UTF-8");
		final PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<body>");
		out.println("<h2>Dieser Service... post-Methode...</h2>");
		out.println("</body>");
		out.println("</html>");
		
}
 
 private void persist(BildBlobBean  bild, Part filepart) throws ServletException {
	 //DB-Zugriff
	 
	 String [] generatedKeys = new String [] {"id"}; // Name der Spalte auto generated
	 try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement(
					 "INSERT INTO bilder (info,bild) VALUES(?,?)", generatedKeys)) {
	
		 pstmt.setString(1, bild.getInfo());
		 pstmt.setBinaryStream(2, filepart.getInputStream());
		 
		 pstmt.executeUpdate();
		 
		 try (ResultSet rs = pstmt.getGeneratedKeys()){
			 while (rs.next()) {
				 bild.setId(rs.getLong(1));
				 
		 }
			 
		 }
	 }catch (Exception ex) {
			 throw new ServletException(ex.getMessage());
		 }
	 
 
 
 
}
}

