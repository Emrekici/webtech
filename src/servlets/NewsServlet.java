//Ersteller: Timo Iwan

package servlets;



import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
//Servlet zum Erstellen von Neuigkeiten

import beans.NewsBean;


@WebServlet("/NewsServlet")


public class NewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		NewsBean newsbean = new NewsBean();
		newsbean.setNewsText(request.getParameter("newsText"));
	
	

		// DB-Zugriff
		persist(newsbean);
		// SESSION
		HttpSession session = request.getSession();
		session.setAttribute("newsbean", newsbean);
		doGet(request, response);
		

		RequestDispatcher rd = request.getRequestDispatcher("jsp/alleNeuigkeiten.jsp");
		rd.forward(request, response);

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		
		HttpSession session = request.getSession();

		List<NewsBean> news = read();
		
		session.setAttribute("newsListe", news);

		
		response.sendRedirect("jsp/alleNeuigkeiten.jsp");		

	}
	


	private void persist(NewsBean newsbean) throws ServletException {
		// DB-Zugriff

		String[] generatedKeys = new String[] { "id" }; // Name der Spalte auto generated
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"INSERT INTO news (newsText) VALUES(?)",
						generatedKeys)) {

			pstmt.setString(1, newsbean.getNewsText());
			

			pstmt.executeUpdate();

			try (ResultSet rs = pstmt.getGeneratedKeys()) {
				while (rs.next()) {
				newsbean.setId(rs.getLong(1));

				}

			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

	}
	
private List<NewsBean> read() throws ServletException {
		
		List<NewsBean> newsList = new ArrayList<NewsBean>();
		
		// DB-Zugriff
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("SELECT * FROM news")) {
			
		
			try (ResultSet rs = pstmt.executeQuery()) {
			
				while (rs.next()) {
					NewsBean news = new NewsBean();
					
					int id = rs.getInt("id");
					news.setId(id);
					
					String text = rs.getString("newsText");
					news.setNewsText(text);
					
				
					newsList.add(news);
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		return newsList;
	}
}

