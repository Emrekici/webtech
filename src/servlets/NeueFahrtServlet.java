// Ersteller: Timo Iwan

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Calendar;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.NeueFahrtBean;

@WebServlet("/NeueFahrtServlet")
@MultipartConfig(location="/tmp")
		
public class NeueFahrtServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource ds;
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	 request.setCharacterEncoding("UTF-8");
		NeueFahrtBean form = new NeueFahrtBean();
		form.setName(request.getParameter("name"));
		form.setKategorie(request.getParameter("kategorie"));
		form.setZielgebiet(request.getParameter("zielgebiet"));
		form.setSeats(request.getParameter("seats"));
		String dateString = request.getParameter("datum");
		String[] dateArray = dateString.split("-");
		Calendar cal = Calendar.getInstance();
		int j = Integer.parseInt(dateArray[0]);
		int m= Integer.parseInt(dateArray[1])-1;
		int t = Integer.parseInt(dateArray[2]);
	    cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    cal.set(Calendar.MILLISECOND, 0);
		cal.set(j, m, t);
		form.setDatum(cal.getTime());	
		
		
		
	
		//DB-Zugriff
		persist(form);
	//SESSION
		HttpSession session = request.getSession();
		session.setAttribute("form", form);
		
		response.sendRedirect("jsp/neueFahrtForm.jsp?status=success");
		
				
}
 
 
 protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");;
		response.setCharacterEncoding("UTF-8");
		final PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<body>");
		out.println("<h2>Dieser Service... post-Methode...</h2>");
		out.println("</body>");
		out.println("</html>");
		
}
 
private void persist(NeueFahrtBean form) throws ServletException {
		// DB-Zugriff

		String[] generatedKeys = new String[] { "id" }; // Name der Spalte auto generated
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"INSERT INTO fahrten (name,kategorie,zielgebiet,datum,seats) VALUES(?,?,?,?,?)",
						generatedKeys)) {

			pstmt.setString(1, form.getName());
			pstmt.setString(2, form.getKategorie());
			pstmt.setString(3, form.getZielgebiet());
			pstmt.setDate(4, new java.sql.Date(form.getDatum().getTime()));
			pstmt.setString(5, form.getSeats());

			pstmt.executeUpdate();

			try (ResultSet rs = pstmt.getGeneratedKeys()) {
				while (rs.next()) {
					form.setId(rs.getInt(1));

				}

			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

 
 
}
}

