// Ersteller: Patrick Eibel

package servlets;

import java.io.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;

import beans.UserBean;



@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;
    
    

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		// Pr�ft ob die eingegebene Emailadresse bereits vergeben ist
		try(Connection con = ds.getConnection();
		PreparedStatement pstmt = con.prepareStatement("SELECT * FROM users WHERE email = ?")) {
			pstmt.setString(1, request.getParameter("email_name"));

			try(ResultSet rs = pstmt.executeQuery()){
				while (rs.next()){
					// Weiterleitung zur�ck zur Registrierung mit Registrierung nicht m�glich
					response.sendRedirect("jsp/register.jsp?register=false");
					return;
				}
			}
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
		
		 UserBean user = new UserBean();
		 user.setAnrede(request.getParameter("anrede"));
		 user.seteMail(request.getParameter("email_name"));
		 user.setVorname(request.getParameter("vorname"));
		 user.setNachname(request.getParameter("nachname"));
		 user.setDatum(request.getParameter("geburtsdatum"));
		 user.setPassword(request.getParameter("password"));
		 user.setVereinsmitglied(request.getParameter("vereinsmitglied"));
		 
		 
		//Nutzer Anlegen und ID zuweisen
			userAnlegen(user, request.getParameter("password"));

		
			// Session erstellen
//			HttpSession session = request.getSession();
//			session.setAttribute("userAccount", user);

			// Weiterleitung - Hauptseite
			response.sendRedirect("http://localhost:8080/demo-wildfly-war/jsp/login.jsp?login=register");
		}
		 
	
	
	private void userAnlegen(UserBean user, String password) throws ServletException, IOException {

		
		// Nutzer in der Datenbank anlegen
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("INSERT INTO users (" +
					 "email, password, Anrede, Vorname, Nachname, Geburtsdatum, Vereinsmitglied" +
					 ") VALUES (?,?,?,?,?,?,?)")) {
			System.out.println(pstmt);
			pstmt.setString(3, user.getAnrede());
			pstmt.setString(1, user.geteMail());
			pstmt.setString(6, user.getDatum());
			pstmt.setString(5, user.getNachname());
			pstmt.setString(2, user.getPassword());
			pstmt.setString(7, user.getVereinsmitglied());
			pstmt.setString(4, user.getVorname());
			
			pstmt.executeUpdate();
			
			
		} catch (Exception e) {
			throw new ServletException(e.getMessage());
		}
	}
	

}
