// Ersteller: Emre Ekici

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.FahrtBean;
import beans.FormularBean;
import beans.UserBean;

@WebServlet("/Buchungsservlet")
public class Buchungsservlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    @Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource ds;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		final HttpSession session = request.getSession();
		request.setCharacterEncoding("UTF-8");	
		
		FormularBean buchung = new FormularBean();
		buchung.setVorname(request.getParameter("vorname"));
		buchung.setNachname(request.getParameter("nachname"));
		buchung.setZielgebiet(request.getParameter("zielgebiet"));
		buchung.setFahrtID(((FahrtBean)session.getAttribute("fahrt")).getId());
		String dateString = request.getParameter("datum");
		String[] dateArray = dateString.split("-");
		Calendar cal = Calendar.getInstance();
		int j = Integer.parseInt(dateArray[0]);
		int m= Integer.parseInt(dateArray[1])-1;
		int t = Integer.parseInt(dateArray[2]);
	    cal.set(Calendar.HOUR_OF_DAY, 0);
	    cal.set(Calendar.MINUTE, 0);
	    cal.set(Calendar.SECOND, 0);
	    cal.set(Calendar.MILLISECOND, 0);
		cal.set(j, m, t);
		buchung.setDatum(cal.getTime());
		buchung.setTageskarte(request.getParameter("tageskarte"));
		buchung.setAusruesstung(request.getParameter("ausruesstung"));
		buchung.setVerreinsbus(request.getParameter("verreinsbus"));
		buchung.setEinstiegsort(request.getParameter("einstiegsort"));
		buchung.setAnmerkung(request.getParameter("anmerkung"));
		
		session.setAttribute("buchung", buchung);
		FahrtBean fahrt = (FahrtBean) session.getAttribute("fahrt");
		

		UserBean user = (UserBean) session.getAttribute("userAccount");
		
		int user_id = (int) user.getID();
		
		persist(buchung, user_id);
		update(fahrt.getSeats(), fahrt.getId());
		

		response.sendRedirect("jsp/Buchungsbestaetigung.jsp");

	}
	
	private void persist(FormularBean buchung, int user_id) throws ServletException {

		String[] generatedKeys = new String[] {"id"};
		
		try (Connection con = ds.getConnection();
			PreparedStatement pstmt = con.prepareStatement(
					"INSERT INTO buchungen (user_id,fahrt_id,vorname,nachname,zielgebiet,datum,tageskarte,"
					+ "ausruesstung,verreinsbus,einstiegsort,anmerkung) VALUES (?,?,?,?,?,?,?,?,?,?,?)", 
					generatedKeys)){
			
			pstmt.setInt(1, user_id);
			pstmt.setInt(2,  buchung.getFahrtID());
			pstmt.setString(3, buchung.getVorname());
			pstmt.setString(4, buchung.getNachname());
			pstmt.setString(5, buchung.getZielgebiet());
			pstmt.setDate(6, new java.sql.Date(buchung.getDatum().getTime()));
			pstmt.setString(7, buchung.getTageskarte());
			pstmt.setString(8, buchung.getAusruesstung());
			pstmt.setString(9, buchung.getVerreinsbus());
			pstmt.setString(10, buchung.getEinstiegsort());
			pstmt.setString(11, buchung.getAnmerkung());
			pstmt.executeUpdate();
			
			try (ResultSet rs = pstmt.getGeneratedKeys()) {
				while (rs.next()) {
					buchung.setId(rs.getInt(1));
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}
	
	private void update(int seats, int id) throws IOException {

		int seatsdec = seats-1;

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("UPDATE fahrten SET seats =? WHERE id =?")) {

			pstmt.setInt(1, seatsdec);
			pstmt.setInt(2, id);
			pstmt.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");	
		
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		final PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<body>");
		out.println("<h2>Dieser Service muss �ber die post-Methode aufgerufen werden.</h2>");
		out.println("</body>");
		out.println("</html>");
	}
    

}

