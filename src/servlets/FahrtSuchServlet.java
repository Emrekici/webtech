// Ersteller: Emre Ekici

package servlets;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import beans.FahrtBean;
import beans.FahrtListeBean;



@WebServlet("/FahrtSuchServlet")
public class FahrtSuchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	private List<FahrtBean> search(String suchFahrt) throws ServletException {
        SimpleDateFormat format2 = new SimpleDateFormat("dd.MM.yyyy");
		List<FahrtBean> fahrten = new ArrayList<FahrtBean>();
		boolean status = false;
		
		
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("SELECT * FROM fahrten WHERE name like ?")) {
			pstmt.setString(1, "%" + suchFahrt + "%");

			try (ResultSet rs = pstmt.executeQuery()) {
				
					
					while (rs.next()) {
						FahrtBean fahrt = new FahrtBean();
						
						String name = rs.getString("name");
						fahrt.setName(name);
						
						String kategorie = rs.getString("kategorie");
						fahrt.setKategorie(kategorie);
						
						String zielgebiet = rs.getString("zielgebiet");
						fahrt.setZielgebiet(zielgebiet);
						
	                    String aDatum = format2.format(rs.getDate("datum"));
	                    fahrt.setaDatum(aDatum);
						
						Date datum = rs.getDate("datum");
						fahrt.setDatum(datum);
						
						int seats = rs.getInt("seats");
						fahrt.setSeats(seats);
						
					    int id = rs.getInt("id");
						fahrt.setId(id);

						fahrten.add(fahrt);
					} 
				
				} 
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		fahrten.sort(Comparator.comparing(FahrtBean::getDatum));
		return fahrten;
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		String suchFahrt = request.getParameter("suche");
		
		List<FahrtBean> fahrt = search(suchFahrt);
		
		request.setAttribute("gefundeneFahrten", fahrt);
		session.setAttribute("gefundeneFahrten", fahrt);

		response.sendRedirect("../../demo-wildfly-war/FahrtListeServlet");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}
}