// Ersteller: Patrick Eibel

package servlets;

import beans.UserBean;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/CheckTokenServlet")
public class CheckTokenServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource ds;

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");

        //Cookies abfragen und speichern
        Cookie [] cookies = request.getCookies();
        UserBean user = new UserBean();
        String cookieName = "token";
        String token = null;
       
        //Pr�fen ob Cookies vorhanden sind
        if(cookies != null) {
            for (Cookie cookie: cookies) {
                if(cookieName.equals(cookie.getName())) {
                    token = cookie.getValue();
                }
            }
            //Pr�fen zu welchen Account der Cookie geh�rt
            if(token != null && isregistered(user, token)){

                //Informationen in Session speichern
                HttpSession session = request.getSession();
                session.setAttribute("userAccount", user);

                //Weiterleiten zur Hauptseite der Person - direkter Login
                
                
                
                RequestDispatcher rd = request.getRequestDispatcher("HauptseiteServlet");
                rd.forward(request, response);
                return;
            }
        }
        //Person hat keinen Cookie - Weiterleitung auf Loginseite
        RequestDispatcher rd = request.getRequestDispatcher("jsp/login.jsp");
        rd.forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();
        writer.println("<!DOCTYPE html>");
        writer.println("<html>");
        writer.println("<body>");
        writer.println("<h2>Dieser Service muss �ber die get-Methode aufgerufen werden.</h2>");
        writer.println("</body>");
        writer.println("</html>");
    }

    private boolean isregistered(UserBean user, String token) {

        boolean isRegistered = false;

        //Pr�fen ob passende Informationen zu dem Cookie hinterlegt sind
        try(Connection con = ds.getConnection();
            PreparedStatement pstmt = con.prepareStatement(
                    "SELECT * FROM users WHERE token = ? ")) {
                           
            pstmt.setString(1, token);

            //Gefundene Informationen in Bean �berf�hren
            try(ResultSet rs = pstmt.executeQuery()){
                if(rs != null && rs.next()) {
                    user.setID(rs.getInt("User_ID"));
                    user.setVorname(rs.getString("Vorname"));
                    user.setNachname(rs.getString("Nachname"));
                    user.seteMail(rs.getString("email"));
                                       }
                    isRegistered = true;
                            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        //Gibt true zur�ck, wenn etwas gefunden wurde, sonst false
        return isRegistered;
    }

}
