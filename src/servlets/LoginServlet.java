// Ersteller: Patrick Eibel

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Base64;
import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import javax.sql.DataSource;

import beans.UserBean;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final SecureRandom secureRandom = new SecureRandom();
	private static final Base64.Encoder base64Encoder = Base64.getUrlEncoder();

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		response.setCharacterEncoding("UTF-8");
		PrintWriter writer = response.getWriter();
		writer.println("<!DOCTYPE html>");
		writer.println("<html>");
		writer.println("<body>");
		writer.println("<h2>Dieser Service muss �ber die post-Methode aufgerufen werden.</h2>");
		writer.println("</body>");
		writer.println("</html>");
	}

	private boolean validate(UserBean userBean) {
		boolean status = false;

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con
						.prepareStatement("SELECT * FROM users WHERE email = ? and password = ? ")) {
			pstmt.setString(1, userBean.geteMail());
			pstmt.setString(2, userBean.getPassword());

			try (ResultSet rs = pstmt.executeQuery()) {
				if (rs != null && rs.next()) {
					userBean.setID(rs.getInt("User_ID"));
					userBean.setRole(rs.getInt("Role"));
					userBean.setVereinsmitglied("Vereinsmitglied");
					userBean.setVorname(rs.getString("vorname"));
					userBean.setNachname(rs.getString("nachname"));
					status = true;
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return status;
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setCharacterEncoding("UTF-8");

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		UserBean userBean = new UserBean();
		userBean.seteMail(username);
		userBean.setPassword(password);

		String stay_logged_in = request.getParameter("checkbox_keep_loggedin");

		if (validate(userBean)) {
			if (stay_logged_in != null) {
				String token = generateNewToken();
				createCookie(true, token, response);
				storeToken(token, userBean);
			} else {
				String token = "";
				createCookie(false, token, response);
			}

			HttpSession session = request.getSession();
			session.setAttribute("userAccount", userBean);

			response.sendRedirect("/demo-wildfly-war/HauptseiteServlet");

		} else {
			response.sendRedirect("jsp/login.jsp?login=false");
		}

	}

	private void createCookie(Boolean logedIn, String token, HttpServletResponse response) {
		if (logedIn) {
			Cookie cookie = new Cookie("token", token);
			cookie.setMaxAge(60 * 60 * 24 * 7);
			response.addCookie(cookie);
		}
		// Cookie erstellen und der Response anf�gen, wenn automatischer Login nicht
		// ausgew�hlt
		if (!logedIn) {
			Cookie cookie = new Cookie("token", token);
			cookie.setMaxAge(0);
			response.addCookie(cookie);
		}
	}

	private void storeToken(String token, UserBean userBean) {
		// Token f�r den jeweiligen Nutzer speichern
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("UPDATE users SET token = ? WHERE User_ID = ?")) {
			pstmt.setString(1, token);
			pstmt.setInt(2, userBean.getID());
			pstmt.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}

	// https://stackoverflow.com/questions/13992972/how-to-create-a-authentication-token-using-java
	// Author: Dimitriy Dumanskiy
	private static String generateNewToken() {
		byte[] randomBytes = new byte[24];
		secureRandom.nextBytes(randomBytes);
		return base64Encoder.encodeToString(randomBytes);
	}
	// Code von Dimitriy Dumanskiy endet
}
