// Ersteller: Emre Ekici

package servlets;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import beans.FahrtBean;
import beans.UserBean;

@WebServlet("/FahrtInfoServlet")
public class FahrtInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	private FahrtBean search(int fahrt_id) throws ServletException {
		boolean status = false;
		FahrtBean fahrt = new FahrtBean();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM fahrten where id = ?")) {
			pstmt.setInt(1, fahrt_id);

			try (ResultSet rs = pstmt.executeQuery()) {

				status = rs.next();

				String name = rs.getString("name");
				fahrt.setName(name);

				String kategorie = rs.getString("kategorie");
				fahrt.setKategorie(kategorie);

				String zielgebiet = rs.getString("zielgebiet");
				fahrt.setZielgebiet(zielgebiet);

				Date datum = rs.getDate("datum");
				fahrt.setDatum(datum);

				int seats = rs.getInt("seats");
				fahrt.setSeats(seats);

				int id = rs.getInt("id");
				fahrt.setId(id);

			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return fahrt;
	}

	private Map<UserBean, Integer> getTeilnehmer(int fahrt_id) throws ServletException {
		boolean status = false;
		Map<UserBean, Integer> teilnehmer = new HashMap<>();

		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement(
						"SELECT u.Vorname, u.Nachname, count(b.vorname) as anzahl FROM users u JOIN buchungen b  ON u.User_ID = b.user_id where b.fahrt_id = ? group by u.User_ID")) {
			pstmt.setInt(1, fahrt_id);

			try (ResultSet rs = pstmt.executeQuery()) {

				while (rs.next()) {
					UserBean user = new UserBean();

					String vorname = rs.getString("vorname");
					user.setVorname(vorname);
					
					String nachname = rs.getString("nachname");
					user.setNachname(nachname);
					
					int anzahl = rs.getInt("anzahl");
					
					teilnehmer.put(user, anzahl);
				}
				

			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}

		return teilnehmer;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();

		int fahrt_id = Integer.valueOf(request.getParameter("fahrt_id"));

		FahrtBean fahrt = search(fahrt_id);
		
		Map<UserBean, Integer> teilnehmer = getTeilnehmer(fahrt_id);
		
		request.setAttribute("teilnehmer", teilnehmer);


		request.setAttribute("fahrt", fahrt);
		session.setAttribute("fahrt", fahrt);
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/fahrt.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}
}
