// Ersteller: Patrick Eibel

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.UserBean;

/**
 * Servlet implementation class editpwServlet
 */
@WebServlet("/editpwServlet")
public class editpwServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	 private DataSource ds;
	String newPassword = "";
	String newPasswordCheck="";
	String oldpw ="";
	String enteredEmail="";
    public editpwServlet() {
        super();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		response.setCharacterEncoding("UTF-8");
		HttpSession session = request.getSession();
		UserBean userBean = (UserBean) request.getSession().getAttribute("userAccount");
		String beanEmail =userBean.geteMail();
		oldpw = request.getParameter("oldpw");
        newPasswordCheck = request.getParameter("newpasswordcheck");
        newPassword = request.getParameter("newpassword");
        enteredEmail = request.getParameter("email");
        String currentPassword = "";
        String currentEmail= "";
        try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM users WHERE email = ? AND password = ?")) {
			pstmt.setString(1, beanEmail) ;
			pstmt.setString(2, oldpw) ;

			try (ResultSet rs = pstmt.executeQuery()) {
				if (rs != null && rs.next()) {
					currentPassword = rs.getString("password");
					currentEmail= rs.getString("email");
				}
			}
		} catch (SQLException e) {

			e.printStackTrace();
		}
        if(newPassword.equals(newPasswordCheck) && oldpw.equals(currentPassword)  && enteredEmail.equals(currentEmail) && !newPassword.equals(oldpw)) {
        	update(userBean, beanEmail, newPassword);
        	session.setAttribute("userAccount", userBean);        	
        	response.sendRedirect("jsp/editpw.jsp?status=success");
        }else {
	response.sendRedirect("jsp/editpw.jsp?status=nomatch");
	
}
	
	}
	
	
	private void update(UserBean userBean, String beanEmail, String newPassword) throws IOException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("UPDATE users" + " SET password = ?" + "WHERE users.email = ?"))  {
			
			

			pstmt.setString(1, newPassword);
			pstmt.setString(2, beanEmail);
			pstmt.executeUpdate();
			userBean.setPassword(newPassword);
			
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}
	
	
	
}