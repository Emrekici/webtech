// Ersteller: Emre Ekici

package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;
import beans.FahrtListeBean;
import beans.FormularBean;
import beans.UserBean;


@WebServlet("/HauptseiteServlet")
public class HauptseiteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	
	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource ds;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");	
		final HttpSession session = request.getSession();
		
		int user_id = ((UserBean)session.getAttribute("userAccount")).getID();
		String kategorie = request.getParameter("kategorie");
	
		List<FahrtListeBean> fahrten = search(kategorie);
		List<FormularBean> buchungen = searchBuchungen(user_id);
		
		System.out.println(buchungen.size());
		session.setAttribute("buchungen", buchungen);
		
		request.setAttribute("fahrten", fahrten);
		session.setAttribute("fahrten", fahrten);

		response.sendRedirect("jsp/hauptseite.jsp");		   
	}
	

	private List<FahrtListeBean> search(String iD) throws ServletException {
		
		List<FahrtListeBean> fahrten = new ArrayList<FahrtListeBean>();
		 SimpleDateFormat format2 = new SimpleDateFormat("dd.MM.yyyy");
		// DB-Zugriff
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("SELECT * FROM fahrten ")) {
			
		
			try (ResultSet rs = pstmt.executeQuery()) {
			
				while (rs.next()) {
					FahrtListeBean fahrtliste = new FahrtListeBean();
					
					Long id1 = rs.getLong("id");
					fahrtliste.setId(id1);
					
					String name = rs.getString("name");
					fahrtliste.setName(name);
					
					String kategorie = rs.getString("kategorie");
					fahrtliste.setKategorie(kategorie);
					
					String zielgebiet = rs.getString("zielgebiet");
					fahrtliste.setZielgebiet(zielgebiet);
					
					String aDatum = format2.format(rs.getDate("datum"));
					fahrtliste.setaDatum(aDatum);
					
					Date datum = rs.getDate("datum");
					
					fahrtliste.setDatum(datum);
					
					String seats = rs.getString("seats");
					fahrtliste.setSeats(seats);
					
					fahrten.add(fahrtliste);
				}
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		fahrten.sort(Comparator.comparing(FahrtListeBean::getDatum));
		return fahrten;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doPost(request, response);
	request.setCharacterEncoding("UTF-8");
		response.setContentType("text/html");;
		response.setCharacterEncoding("UTF-8");
		final PrintWriter out = response.getWriter();
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<body>");
		out.println("<h2>Dieser Service... post-Methode...</h2>");
		out.println("</body>");
		out.println("</html>");
		
	}
	
	private List<FormularBean> searchBuchungen(int user_id) throws ServletException {
		List<FormularBean> buchungen = new ArrayList<FormularBean>();
		
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("SELECT * FROM buchungen where user_id = ?")) {
			pstmt.setInt(1, user_id);

			try (ResultSet rs = pstmt.executeQuery()) {
			
				while (rs.next()) {
					FormularBean buchung = new FormularBean();
					
				    int id = rs.getInt("id");
					buchung.setId(id);
					
					String vorname = rs.getString("vorname");
					buchung.setVorname(vorname);
					
					String nachname = rs.getString("nachname");
					buchung.setNachname(nachname);
					
					String zielgebiet = rs.getString("zielgebiet");
					buchung.setZielgebiet(zielgebiet);

					Date datum = rs.getDate("datum");
					buchung.setDatum(datum);
					
					String tageskarte = rs.getString("tageskarte");
					buchung.setTageskarte(tageskarte);
					
					String ausruesstung = rs.getString("ausruesstung");
					buchung.setAusruesstung(ausruesstung);
					
					String verreinsbus = rs.getString("verreinsbus");
					buchung.setVerreinsbus(verreinsbus);
					
					String einstiegsort = rs.getString("einstiegsort");
					buchung.setEinstiegsort(einstiegsort);
					
					String anmerkung = rs.getString("anmerkung");
					buchung.setAnmerkung(anmerkung);
					
					buchungen.add(buchung);
				} 
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		return buchungen;
	}

	

}