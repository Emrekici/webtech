// Ersteller: Timo Iwan


//Servlet zur Ausgabe der Bilder aus Liste
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.BildBean;




/**
 * Servlet implementation class FahrtListe
 */
@WebServlet("/BildServletListe")
public class BildServletListe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
    private DataSource ds;
	
	private List<BildBean> search(String iD) throws ServletException {
		
		List<BildBean> bilder = new ArrayList<BildBean>();
		
		// DB-Zugriff
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("SELECT * FROM bilder ")) {
			
		
			try (ResultSet rs = pstmt.executeQuery()) {
			
				while (rs.next()) {
					BildBean bild = new BildBean();
					
					Long id = rs.getLong("id");
					bild.setId(id);
					bild.setInfo(rs.getString("info"));
					
					
					
				
					bilder.add(bild);
				} // while rs.next()
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		return bilder;
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");	
		final HttpSession session = request.getSession();
		
		
		String id = request.getParameter("id");
		
	
		List<BildBean>  bilder = search(id);
		// Scope "Request"
				request.setAttribute("bilder", bilder);
				session.setAttribute("bilder", bilder);
				
				
				
			
				response.sendRedirect("jsp/bildAnzeigen.jsp");
			
//		// Weiterleiten an JSP
//				final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/bildAnzeigen.jsp");
//				dispatcher.forward(request, response);	
//			   
	}
	

	
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	doGet(request, response);
	
	}

	

}
