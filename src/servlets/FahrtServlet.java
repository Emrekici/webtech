// Ersteller:Emre Ekici

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.FahrtBean;

@WebServlet("/FahrtServlet")
public class FahrtServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		
		int fahrt_id = Integer.valueOf(request.getParameter("id"));
		Long id = Long.valueOf(request.getParameter("id"));
		HttpSession session = request.getSession();
		// DB-Zugriff
		FahrtBean fahrt = search(fahrt_id).get(0);
		read(id);

		request.setAttribute("fahrt_id", fahrt_id);
		session.setAttribute("fahrt", fahrt);
	
		response.sendRedirect("jsp/Buchungsformular.jsp");
	}
	private void read(Long id) throws ServletException {
		// DB-Zugriff
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("SELECT * FROM fahrten WHERE id = ?")) {
			pstmt.setLong(1, id);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}
	
	private List<FahrtBean> search(long fahrt_id) throws ServletException {
		List<FahrtBean> fahrten = new ArrayList<FahrtBean>();
		
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("SELECT * FROM fahrten where id = ?")) {
			pstmt.setLong(1, fahrt_id);

			try (ResultSet rs = pstmt.executeQuery()) {
			
				while (rs.next()) {
					FahrtBean fahrt = new FahrtBean();
					
					String name = rs.getString("name");
					fahrt.setName(name);
					
					String kategorie = rs.getString("kategorie");
					fahrt.setKategorie(kategorie);
					
					String zielgebiet = rs.getString("zielgebiet");
					fahrt.setZielgebiet(zielgebiet);
					
					Date datum = rs.getDate("datum");
					fahrt.setDatum(datum);
					
					int seats = rs.getInt("seats");
					fahrt.setSeats(seats);
					
				    int id = rs.getInt("id");
					fahrt.setId(id);
					
					
					fahrten.add(fahrt);
				} 
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		return fahrten;
	}
	
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}