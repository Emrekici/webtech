// Ersteller: Emre Ekici

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.FormularBean;
import beans.UserBean;

@WebServlet("/TeilnehmerServlet")
public class TeilnehmerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;
	


	private List<FormularBean> search(int user_id) throws ServletException {
		List<FormularBean> buchungen = new ArrayList<FormularBean>();
		
		try (Connection con = ds.getConnection();
			 PreparedStatement pstmt = con.prepareStatement("SELECT * FROM buchungen where user_id = ?")) {
			pstmt.setInt(1, user_id);

			try (ResultSet rs = pstmt.executeQuery()) {
			
				while (rs.next()) {
					FormularBean buchung = new FormularBean();
					
				    int id = rs.getInt("id");
					buchung.setId(id);
					
					String vorname = rs.getString("vorname");
					buchung.setVorname(vorname);
					
					String nachname = rs.getString("nachname");
					buchung.setNachname(nachname);
					
					String zielgebiet = rs.getString("zielgebiet");
					buchung.setZielgebiet(zielgebiet);

					Date datum = rs.getDate("datum");
					buchung.setDatum(datum);
					
					String tageskarte = rs.getString("tageskarte");
					buchung.setTageskarte(tageskarte);
					
					String ausruesstung = rs.getString("ausruesstung");
					buchung.setAusruesstung(ausruesstung);
					
					String verreinsbus = rs.getString("verreinsbus");
					buchung.setVerreinsbus(verreinsbus);
					
					String einstiegsort = rs.getString("einstiegsort");
					buchung.setEinstiegsort(einstiegsort);
					
					String anmerkung = rs.getString("anmerkung");
					buchung.setAnmerkung(anmerkung);
					
					buchungen.add(buchung);
				} 
			}
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
		
		return buchungen;
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding("UTF-8");
		
		final HttpSession session = request.getSession();
		
		UserBean user = (UserBean) session.getAttribute("userAccount");
		
		int user_id = (int) user.getID();
		
		
		List<FormularBean> buchungen = search(user_id);
				
		request.setAttribute("buchungen", buchungen);
		
		final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/Buchungsliste.jsp");
		dispatcher.forward(request, response);	
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}