//Ersteller: Emre Ekici

package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.FormularBean;


@WebServlet("/BuchungLöschen")
public class BuchungLöschen extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup = "java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");
		final HttpSession session = request.getSession();
		
		Long id = Long.valueOf(request.getParameter("id"));
		
		List<FormularBean> buchungen = (List<FormularBean>) session.getAttribute("buchungen");
		buchungen.removeIf(b -> b.getId() == id);
		session.setAttribute("buchungen", buchungen);
		
		update(id);

		delete(id);

		// Scope "Request"
		request.setAttribute("id", id);
		

		// Weiterleiten an JSP
		response.sendRedirect("jsp/profileinstellungen.jsp");
	}

	private void delete(Long id) throws ServletException {
		// DB-Zugriff
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("DELETE FROM buchungen WHERE id = ?")) {
			pstmt.setLong(1, id);
			pstmt.executeUpdate();
		} catch (Exception ex) {
			throw new ServletException(ex.getMessage());
		}
	}

	private void update(Long id) throws IOException {
		try (Connection con = ds.getConnection();
				PreparedStatement pstmt = con.prepareStatement("UPDATE fahrten SET seats =seats+1 WHERE id =(select fahrt_id from buchungen where id =?)")) {

			pstmt.setLong(1, id);
			pstmt.executeUpdate();
		} catch (SQLException throwables) {
			throwables.printStackTrace();
		}
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}
}
