// Ersteller Timo Iwan


//Servlet zur Anzeige, Umleitung �ber BildServletGet
package servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import beans.BildBean;


@WebServlet("/BildServlet")
public class BildServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(lookup="java:jboss/datasources/MySqlThidbDS")
	private DataSource ds;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Servlet zum Auslesen eines Bildes aus einer DB und R�ckgabe als bin�rer Datenstrom
		request.setCharacterEncoding("UTF-8");	// In diesem Format erwartet das Servlet jetzt die Formulardaten
		Long id = Long.valueOf(request.getParameter("id"));
		
		// Bean Generierung
//		BildBean bildbean = new BildBean();
//		bildbean.setId(id);
//		bildbean.setInfo(request.getParameter("info"));
		BildBean bildbean = read(id);
		
		
//	bildbean.setId(id);
		//Scope "request"
final HttpSession session = request.getSession();
	
		session.getAttribute("id");
		session.setAttribute("id", id);
		session.setAttribute("bildbean", bildbean);
		request.setAttribute("bildbean", bildbean);

		//Weiterleitung JSP
	final RequestDispatcher dispatcher = request.getRequestDispatcher("jsp/profileinstellungen.jsp");
dispatcher.forward(request, response);
		
	}
		private BildBean read(Long id) throws ServletException {
			BildBean bildbean = new BildBean();
			bildbean.setId(id);

			// DB-Zugriff
			try (Connection con = ds.getConnection();
				 PreparedStatement pstmt = 
						 con.prepareStatement("SELECT info "
						 		            + "FROM bilder "
						 		            + "WHERE id = ?")) {
				
				pstmt.setLong(1, id);
				try (ResultSet rs = pstmt.executeQuery()) {
					if (rs != null && rs.next()) {
						
					//bildbean.setId(rs.getLong("id"));
					bildbean.setInfo(rs.getString("info"));
					}
				}
			} catch (Exception ex) {
				throw new ServletException(ex.getMessage());
			}
			
			return bildbean;
		}

	



	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
