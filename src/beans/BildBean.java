// Ersteller: Timo Iwan

package beans;

import java.io.Serializable;


public class BildBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	private String info;

	private byte[] bild;
public BildBean() {
		
	}
	public BildBean(Long id, String info) {
		super();
		this.id = id;
		this.info = info;
		
	
		
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public byte[] getBild() {
		return bild;
	}
	public void setBild(byte[] bild) {
		this.bild = bild;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
}