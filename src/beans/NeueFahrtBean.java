// Ersteller: Timo Iwan

package beans;

import java.io.Serializable;
import java.util.Date;

public class NeueFahrtBean implements Serializable {
	
	private static final long serialVersionUID =1L;
	
	
	private String name;
	private String kategorie;
	private String zielgebiet;
	private Date datum;
	private String seats;

	private int id;
	

	
	
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKategorie() {
		return kategorie;
	}
	public void setKategorie(String kategorie) {
		this.kategorie = kategorie;
	}
	public String getZielgebiet() {
		return zielgebiet;
	}
	public void setZielgebiet(String zielgebiet) {
		this.zielgebiet = zielgebiet;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getSeats() {
		return seats;
	}
	public void setSeats(String seats) {
		this.seats = seats;
	}
	

	
	}




