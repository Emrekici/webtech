// Ersteller: Patrick Eibel

package beans;

import java.io.Serializable;

public class UserBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private String eMail;
	private String password;
	private String vorname;
	private String anrede;
	private String nachname;
	private String datum;
	private String vereinsmitglied;
	private int id;
	private int role;
	
	public int getRole() {
		return role;
	}
	
	public void setRole(int role) {
		this.role = role;
	}
	
	public int getID() {
		return id;
	}
	
	public void setID(int id) {
		this.id = id;
	}
	
	public String geteMail() {
		return eMail;
	}
	public void seteMail(String eMail) {
		this.eMail = eMail;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
		
	}
	
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
		
	}
	
	public String getAnrede() {
		return anrede;
	}
	public void setAnrede(String anrede) {
		this.anrede = anrede;
		
	}
	
	public String getDatum() {
		return datum;
	}
	public void setDatum(String datum) {
		this.datum = datum;
		
	}
	
	public String getVereinsmitglied() {
		return vereinsmitglied;
	}
	public void setVereinsmitglied(String vereinsmitglied) {
		this.vereinsmitglied = vereinsmitglied;
		
	}


	
	
	
	

	
}

