// Ersteller: Timo Iwan

package beans;

import java.io.Serializable;


public class BildBlobBean implements Serializable {
	
	private static final long serialVersionUID =1L;
	
	
	private String info;
	private String bildName;
	private Long id;
	

public BildBlobBean() {
		
	}
	public BildBlobBean(Long id, String info, String bildName) {
		super();
		this.id = id;
		this.info = info;
		this.bildName = bildName;
	
		
	}
	
	
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}

	public void setBildName(String setBildName) {
		this.bildName = setBildName;
		
	}
	public String getBildName() {
		return bildName;
	}
	

	
	}




