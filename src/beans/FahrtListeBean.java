// Ersteller: Timo Iwan

package beans;

import java.io.Serializable;
import java.util.Date;

public class FahrtListeBean implements Serializable {
    private static final long serialVersionUID = 1L;


    private String name;
    private String kategorie;
    private String zielgebiet;
    private Date datum;
    private String aDatum;

    private String seats;

    private Long id;

    public FahrtListeBean() {

    }
    public FahrtListeBean(Long id, String name, String kategorie, String zielgebiet, Date datum, String seats, String aDatum) {
        super();
        this.id = id;
        this.name = name;
        this.kategorie = kategorie;
        this.zielgebiet = zielgebiet;
        this.datum = datum;
        this.seats = seats;
        this.aDatum = aDatum;

    }

    public String getaDatum() {
        return aDatum;
    }
    public void setaDatum(String aDatum) {
        this.aDatum = aDatum;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getKategorie() {
        return kategorie;
    }
    public void setKategorie(String kategorie) {
        this.kategorie = kategorie;
    }
    public String getZielgebiet() {
        return zielgebiet;
    }
    public void setZielgebiet(String zielgebiet) {
        this.zielgebiet = zielgebiet;
    }
    public Date getDatum() {
        return datum;
    }
    public void setDatum(Date datum) {
        this.datum = datum;
    }
    public Long getId() {
        return id;
    }
    public void setId(Long id1) {
        this.id = id1;
    }
    public String getSeats() {
        return seats;
    }
    public void setSeats(String seats) {
        this.seats = seats;
    }


    }