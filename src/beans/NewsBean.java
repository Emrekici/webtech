// Ersteller: Timo Iwan

package beans;

import java.io.Serializable;


public class NewsBean implements Serializable{
	
		
		private static final long serialVersionUID =1L;
		
		
		private String newsText;
		private long id;
		
		public NewsBean() {
			
		}
		public NewsBean(Long id, String newsText) {
			super();
			this.id = id;
			this.newsText = newsText;
			
			
		}
		
		public long getId() {
			return id;
		}
		public void setId(long id) {
			this.id = id;
		}
		public String getNewsText() {
			return newsText;
		}
		public void setNewsText(String newsText) {
			this.newsText = newsText;
		}
		

		
		
		
		

	




}
