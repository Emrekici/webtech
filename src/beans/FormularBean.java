// Ersteller: Emre Ekici

package beans;

import java.io.Serializable;
import java.lang.String;
import java.util.Date;


public class FormularBean implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private int id;
	private int fahrt_id;
	private String vorname;
	private String nachname;
	private String zielgebiet;
	private Date   datum;  
	private String tageskarte;
	private String ausruesstung;
	private String verreinsbus;
	private String einstiegsort;
	private String anmerkung;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getVorname() {
		return vorname;
	}
	public void setVorname(String vorname) {
		this.vorname = vorname;
	}
	public String getNachname() {
		return nachname;
	}
	public void setNachname(String nachname) {
		this.nachname = nachname;
	}
	public String getZielgebiet() {
		return zielgebiet;
	}
	public void setZielgebiet(String zielgebiet) {
		this.zielgebiet = zielgebiet;
	}
	public Date getDatum() {
		return datum;
	}
	public void setDatum(Date datum) {
		this.datum = datum;
	}
	public String getTageskarte() {
		return tageskarte;
	}
	public void setTageskarte(String tageskarte) {
		this.tageskarte = tageskarte;
	}
	public String getAusruesstung() {
		return ausruesstung;
	}
	public void setAusruesstung(String ausruesstung) {
		this.ausruesstung = ausruesstung;
	}
	public String getVerreinsbus() {
		return verreinsbus;
	}
	public void setVerreinsbus(String verreinsbus) {
		this.verreinsbus = verreinsbus;
	}
	public String getEinstiegsort() {
		return einstiegsort;
	}
	public void setEinstiegsort(String einstiegsort) {
		this.einstiegsort = einstiegsort;
	}
	public String getAnmerkung() {
		return anmerkung;
	}
	public void setAnmerkung(String anmerkung) {
		this.anmerkung = anmerkung;
	}
	public int getFahrtID() {
		return fahrt_id;
	}
	public void setFahrtID(int fahrt_id) {
		this.fahrt_id = fahrt_id;
	}
	
}
