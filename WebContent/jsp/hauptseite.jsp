<!-- Ersteller: Timo Iwan -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>

<!DOCTYPE html>
<html>


<head>

<meta charset="UTF-8">
<title>SWC Wolnzach</title>
<link href="../css/css_hauptseite.css" rel="stylesheet" type="text/css">
</head>


<base href="${pageContext.request.requestURI}" />


<header>
	<%@ include file="../jspf/Header.jspf"%>
</header>


<jsp:useBean id="now" class="java.util.Date" />


<c:choose>

	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>


	<c:otherwise>

		<body>
			<div class="headers">
				<h1>
					<a href="kommendeFahrtenÜbersicht.jsp"><b>kommende Touren</b></a>

				</h1>
			</div>

			<div class="fahrtlinks">
				<c:forEach var="fahrti" items="${fahrten}" varStatus="status">
					<form id="Form1" method="get" action="../FahrtServlet">
						<c:if test="${fahrti.datum gt now || fahrti.datum eq now}">
							<c:choose>
								<c:when test="${fahrti.kategorie eq 'wanderung'}">
									<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img
										src="../img/wanderungImage.png" alt="bild"></a>
									<div class="centered">
										<div class="box">${fahrti.name}<br>${fahrti.aDatum}<br>
											<c:choose>
												<c:when test="${fahrti.seats gt 0}">
												Freie Plätze: ${fahrti.seats}
													<span class="dot1"></span>
												</c:when>
												<c:when test="${fahrti.seats eq 0}">
													Leider ausgebucht
													<span class="dot3"></span>
												</c:when>
											</c:choose>

										</div>
									</div>
								</c:when>
								<c:otherwise>
									<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img
										src="../img/skiImage.png" alt="bild"></a>
									<div class="centered">
										<div class="box">${fahrti.name}<br>${fahrti.aDatum}<br>
											<c:choose>
												<c:when test="${fahrti.seats gt 0}">
												Freie Plätze: ${fahrti.seats}
													<span class="dot1"></span>
												</c:when>
												<c:when test="${fahrti.seats eq 0}">
													Leider ausgebucht
													<span class="dot3"></span>
												</c:when>
											</c:choose>

										</div>
									</div>
								</c:otherwise>
							</c:choose>
						</c:if>
					</form>
				</c:forEach>
			</div>
			<div class="headers">
				<h2>
					<a href="vergangeneFahrtenÜbersicht.jsp"> <b>vergangene
							Touren</b></a>
				</h2>
			</div>


			<div class="fahrtlinks">
				<c:forEach var="fahrti" items="${fahrten}" varStatus="status">
					<form id="Form2" method="get" action="../FahrtServlet">
						<c:if test="${fahrti.datum lt now}">
							<c:choose>
								<c:when test="${fahrti.kategorie eq 'wanderung'}">
									<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img
										src="../img/wanderungImage.png" alt="bild"></a>
									<div class="centered">
										<div class="box">${fahrti.name}<br>${fahrti.aDatum}<br>
										</div>

									</div>
								</c:when>
								<c:otherwise>
									<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img
										src="../img/skiImage.png" alt="bild"></a>
									<div class="centered">
										<div class="box">${fahrti.name}<br>${fahrti.aDatum}<br>
										</div>

									</div>
								</c:otherwise>
							</c:choose>
						</c:if>
					</form>
				</c:forEach>
			</div>

			<div class="headers">
				<h3>
					<b>Mitgliederbereich</b>
				</h3>
			</div>
			<p>


				<a href="../NewsServlet"><img class="img"
					src="../img/bild7.png" alt="logo"></a> <a
					href="profileinstellungen.jsp"><img class="img"
					src="../img/bild8.png" alt="logo"></a>
			</p>
			<hr>
		</body>
	</c:otherwise>
</c:choose>
</html>
