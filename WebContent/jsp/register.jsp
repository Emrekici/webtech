<!-- Ersteller: Patrick Eibel -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="UTF-8">
<title>Registrierung</title>
<base href="${pageContext.request.requestURI}" />
<script type="text/javascript" src="../js/register.js"></script>
<link rel="stylesheet" type="text/css" href="../css/FormularStyle.css">
</head>
<header>
	<%@ include file="../jspf/HeaderLogin.jspf"%>
</header>
<body>
<section>
<form action="../RegisterServlet" method="post">
		<label>Anrede</label> 
		<select class="form-control" name="anrede">
			<option value="Frau">Frau</option>
			<option value="Herr">Herr</option>
		</select>


		<label>Vorname</label> 
		<input type="text" class="form-control" name="vorname" placeholder="Vorname" required> 
		<label>Nachname</label>
		<input type="text" class="form-control" name="nachname" placeholder="Nachname"> <label>Geburtsdatum</label> <input
			type="date" class="" name="geburtsdatum" placeholder="Geburtsdatum" required>


		<label>Email</label> <input type="email" class="form-control"
			name="email_name" placeholder="Email" required> <label>Passwort</label>
		<input type="password" class="form-control" name="password"
			placeholder="Passwort" required> <label>Vereinsmitglied</label> <select
			class="form-control" name="vereinsmitglied" required>
			<option value="JA">JA</option>

			<option value="Nein">NEIN</option>
		</select> <input type="submit" class="btn btn-primary" name="registerbutton"
			value="Registrieren">
	<p id="warning"></p>
</form>
</section>
</body>
</html>
