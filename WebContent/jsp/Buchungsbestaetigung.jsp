﻿<!-- Ersteller: Emre Ekici -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="c" 
	uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="de">
	<head>
		<meta charset="UTF-8">
		<link href="../css/FormularStyle.css" rel="stylesheet">
		<title>Buchungsbestätigung</title>
	</head>
	
	<body>
		<header>
			<%@ include file="../jspf/Header.jspf" %> 
		</header>
		
			<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>
	<c:otherwise>
		<form id="myForm" method="get" action="../Buchungsliste" accept-charset="utf-8">
			<h2>Ihre Buchung war erfolgreich</h2>
			<h3>Ihre Formulareingaben</h3>
			<br><b>Vorname: </b>${buchung.vorname}
			<br><b>Nachname: </b>${buchung.nachname}
			<br><b>Skigebiet: </b>${buchung.zielgebiet}
			<br><b>Tageskarte: </b>${buchung.tageskarte}
			<br><b>Ausrüsstung: </b>${buchung.ausruesstung}
			<br><b>Verreinsbus: </b>${buchung.verreinsbus}
			<br><b>Einstiegsort: </b>${buchung.einstiegsort}
			<br><b>Ihre Anmerkungen: </b>${buchung.anmerkung}
			<br><button type="submit" name="alleBuchungen" value="alleBuchungen" formaction="profileinstellungen.jsp">Alle Buchungen anzeigen</button>
		</form>
		<footer>
			<%@ include file="../jspf/Footer.jspf" %> 
		</footer>
			</c:otherwise>
	</c:choose>	
	</body>
</html>


