<!-- Ersteller: Patrick Eibel -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="UTF-8">
<title>Passwort ändern</title>
<base href="${pageContext.request.requestURI}" />
<script type="text/javascript" src="../js/editpw.js"></script>
<script type="text/javascript" src="../js/cookie.js"></script>
<link rel="stylesheet" type="text/css" href="../css/Login.css">
</head>
<body>


    <header>

        <%@ include file="../jspf/Header.jspf"%>
    </header>
    <c:choose>

        <c:when test="${userAccount == null && cookie.token == null}">
            <c:redirect
                url="login.jsp?login=noLog" />
        </c:when>


        <c:otherwise>
            <form action="../editpwServlet" method="post">
                <div class="section input">
                    <input type="email" class="form-control" name="email"
                        placeholder="Email" required> <input type="password"
                        class="form-control" name="oldpw" placeholder="aktuelles Passwort" required>
                    <input type="password" class="form-control" name="newpassword"
                        placeholder="neues Passwort" required> <input type="password"
                        class="form-control" name="newpasswordcheck"
                        placeholder="neues Passwort bestätigen" required>
                    <button type="submit" name="changepw">Passwort ändern</button>
                    <p id="warning"></p>


                </div>
            </form>
        </c:otherwise>
    </c:choose>

    <footer>
        <%@ include file="../jspf/Footer.jspf"%>
    </footer>
</body>

</html>