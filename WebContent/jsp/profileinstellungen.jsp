<!-- Ersteller: Timo Iwan -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>SWC Wolnzach</title>
<link href="../css/css_hauptseite.css" rel="stylesheet">
<base href="${pageContext.request.requestURI}" />
<script type="text/javascript" src="../js/uploadcheck.js"></script>

<link href="../css/FormularStyle.css" rel="stylesheet">

</head>
<header>
	<%@ include file="../jspf/Header.jspf"%>
	</header>
<body>
		
<c:choose>
<c:when test="${userAccount == null && cookie.token == null}">
				<c:redirect
					url="login.jsp?login=noLog" />
			</c:when>
			
		<c:when test="${userAccount.role == 0}">
	<div class="headers">
		<h2>Benutzerverwaltung</h2>
		
	</div>
	
		
	
	<div class ="abstand">
		<br><b>Vorname: </b>${userAccount.vorname}
		<br><b>Nachname: </b>${userAccount.nachname}
		<br><b>Email: </b>${userAccount.eMail}
	<form method="post">
	<button type="submit" name="passwortZurücksetzen"
			value="passwortZurücksetzen" formaction="editpw.jsp">Passwort
			zurücksetzen</button>
			</form>

</div>



	<div class="headers">
		<h2>Erinnerungsbild hochladen</h2>
	</div>
	<form id="FormularStyle" method="post" action="../BildBlobServlet"
		accept-charset="utf-8" enctype="multipart/form-data" onsubmit="uploadcheck()">



		<div class="abstand">

				<div id="check"></div>

			<textarea id="info" name="info"
				placeholder="Informationen zur Tour..." cols="35" rows="4" required></textarea>
			<br> <input id="bild" placeholder ="" name="bild" type="file" accept="image/*"
				required> <br>

			<button type="submit" name="anlegen" value="anlegen">Erinnerung hochladen</button>
			

		</div>

	</form>

	<div class="headers">
		<h3>Hochgeladene Bilder</h3>
	</div>

	<div class="abstand">
		<form id="FormularStyle" method="post" action="../BildServletListe"
			accept-charset="utf-8">

			<button type="submit"
				onclick="window.location.href = '../jsp/bildAnzeigen.jsp';">
				Erinnerungen anzeigen</button>
		</form>
	</div>




	<!-- Ersteller:  Emre Ekici -->
	<div class="headers">
		<h3>Ihre Buchungen</h3>
	</div>
	<div class="abstand">
		<c:forEach var="buchung" items="${buchungen}" varStatus="status">
			<form id="myForm" method="get" action="../BuchungLöschen">
				<br> <input value="${buchung.id}" name="id" type="hidden"></input>
				<br> <b>Vorname: </b>${buchung.vorname} <br> <b>Nachname:
				</b>${buchung.nachname} <br> <b>Zielgebiet: </b>${buchung.zielgebiet}
				<br> <b>Datum: </b>${buchung.datum} <br> <b>Tageskarte:
				</b>${buchung.tageskarte} <br> <b>Ausrüsstung: </b>${buchung.ausruesstung}
				<br> <b>Verreinsbus: </b>${buchung.verreinsbus} <br> <b>Einstiegsort:
				</b>${buchung.einstiegsort} <br> <b>Ihre Anmerkungen: </b>${buchung.anmerkung}
				<br>
				<button type="submit" value="löschen">Buchung stornieren</button>
			</form>
		</c:forEach>
	</div>
	</c:when>
	
	<c:otherwise>
		<div class="headers">
		<h2>AdminDashboard</h2>
		
	</div>
	
	
	
	<div class ="abstand">
		<br><b>Vorname: </b>${userAccount.vorname}
        <br><b>Nachname: </b>${userAccount.nachname}
        <br><b>Email: </b>${userAccount.eMail}
	<form method="post">
	<button type="submit" name="passwortZurücksetzen"
			value="passwortZurücksetzen" formaction="editpw.jsp">Passwort
			zurücksetzen</button>
			</form>
	</div>
<!-- TImo IWan -->	
<div class ="abstand">
	<button onclick="window.location.href = '../jsp/neueFahrtForm.jsp';"> Fahrt erstellen</button>
	</div>
	<div class="headers">
		<h2>Erinnerungsbild hochladen</h2>
	</div>
	<form id="FormularStyle" method="post" action="../BildBlobServlet"
		accept-charset="utf-8" enctype="multipart/form-data">



		<div class="abstand">

					<p id="erfolg"></p>

			<textarea id="info" name="info"
				placeholder="Informationen zur Tour..." cols="35" rows="4" required></textarea>
			<br> <input id="bild" name="bild" type="file" accept="image/*"
				required> <br>

			<button type="submit">Erinnerung hochladen</button>

		</div>

	</form>

	<div class="headers">
		<h3>Hochgeladene Bilder</h3>
	</div>

	<div class="abstand">
		<form id="FormularStyle" method="post" action="../BildServletListe"
			accept-charset="utf-8">

			<button type="submit"
				onclick="window.location.href = '../jsp/bildAnzeigen.jsp';">
				Erinnerungen anzeigen</button>
		</form>
	</div>


	<!-- Ersteller:  Emre Ekici -->
	<div class="headers">
		<h3>Ihre Buchungen</h3>
	</div>
	<div class="abstand">
		<c:forEach var="buchung" items="${buchungen}" varStatus="status">
			<form id="myForm" method="get" action="../BuchungLöschen">
				<br> <input value="${buchung.id}" name="id" type="hidden"></input>
				<br> <b>Vorname: </b>${buchung.vorname} <br> <b>Nachname:
				</b>${buchung.nachname} <br> <b>Zielgebiet: </b>${buchung.zielgebiet}
				<br> <b>Datum: </b>${buchung.datum} <br> <b>Tageskarte:
				</b>${buchung.tageskarte} <br> <b>Ausrüsstung: </b>${buchung.ausruesstung}
				<br> <b>Verreinsbus: </b>${buchung.verreinsbus} <br> <b>Einstiegsort:
				</b>${buchung.einstiegsort} <br> <b>Ihre Anmerkungen: </b>${buchung.anmerkung}
				<br>
				<button type="submit" value="löschen">Buchung stornieren</button>
			</form>
		</c:forEach>
	</div>
	</c:otherwise>
	</c:choose>
</body>
<footer>
	<%@ include file="../jspf/Footer.jspf"%>
</footer>
</html>