<!-- Ersteller: Timo Iwan -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" 
	uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>

<html>
	<head>

		<meta charset="UTF-8">
		
		<title>NeueFahrtAusgabe</title>
		<base href="$pageContext.request.requestURI}"/>
		
	</head>
	<body>
					 	<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>
	<c:otherwise>
	<header>
	<%@ include file="../jspf/Header.jspf"%>
</header>
		

		<br><b>Name: ${form.name}</b>
		<br><b>Kategorie: ${form.kategorie}</b>
		<br><b>Zielgebiet: ${form.zielgebiet}</b>
		<br><b>Datum: ${form.datum}</b>
		<br><b>Plätze: ${form.seats}</b>
		<br><b>ID: ${form.id}</b>
		
	
		
	<footer>
		<%@ include file="../jspf/Footer.jspf" %> 
	</footer>
		</c:otherwise>
	</c:choose>	
	</body>
</html>