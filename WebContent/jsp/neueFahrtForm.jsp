<!-- Ersteller: Timo Iwan -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>neueFahrtFormular</title>
<link href="../css/FormularStyle.css" rel="stylesheet">
<link href="../css/css_hauptseite.css" rel="stylesheet">
<script src="../js/uploadcheck2.js"></script>

<body>
				 	<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>
	<c:otherwise>
<header>
	<%@ include file="../jspf/Header.jspf"%>
</header>



<form id="neuefahrt" method="post" action="../NeueFahrtServlet" accept-charset="utf-8" enctype="multipart/form-data">
	
	<div class="headers"><h1>Neue Fahrt erstellen</h1></div>
	<div class="abstand">
	<div>
	<label for="name">Name:</label>
	<input id="name" name="name" type="text" maxlength="16" required>
	</div>
	<div>
	<label for="kategorie">Kategorie:</label>
	<select name="kategorie" id="kategorie" required>
					<option value="" disabled selected>--Auswählen--</option>
					<option value="skifahrt">Skifahrt</option>
					<option value="wanderung">Wanderung</option>
					
				</select>
	</div>
	<div>
	<label for="zielgebiet">Zielgebiet:</label>
	<input id="zielgebiet" name="zielgebiet" type="text" required>
	</div>
	<div>
	<label for="datum">Datum:</label>
	<input id="datum" name="datum" type="date" required>
	 </div>
	 <div>
	<label for="seats">Plätze:</label>
	<input id="seats" name="seats" type="number" required>
	</div>
	<p id="warning"></p>

	<div>
	<button type="submit" name="anlegen" value="anlegen"> Anlegen</button>
	</div>
	
	
</div>


</form>
	<footer>
		<%@ include file="../jspf/Footer.jspf" %> 
	</footer>
		</c:otherwise>
	</c:choose>	
</body>
</html>