<!-- Ersteller: Patrick Eibel -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="UTF-8">
<title>Login</title>
<base href="${pageContext.request.requestURI}" />
<script type="text/javascript" src="../js/login.js"></script>
<link rel="stylesheet" type="text/css" href="../css/Login.css">
</head>

<body>




<!-- check cookie or session -->  
	<c:choose>
		<c:when test="${userAccount != null }">
			<c:redirect
			url="http://localhost:8080/demo-wildfly-war/jsp/hauptseite.jsp" />
		</c:when>
		<c:when test="${cookie.token != null}">
			<c:redirect
			url="http://localhost:8080/demo-wildfly-war/index.jsp" />
		</c:when>
		<c:otherwise>
			<header>
			<%@ include file="../jspf/HeaderLogin.jspf"%>
			</header>

		<div class="loginContent">
			<noscript>
				<div id="js_warning">
					<b>JavaScript deaktiviert!<br>Für die Nutzung dieser
						Webseite ist JavaScript zwingend notwendig.
					</b>
				</div>

			</noscript>
		</div>
		<h1>Anmeldung</h1>
		<form action="../LoginServlet" method="post">
			<input type="text" name="username" placeholder="Username"> <input
				type="password" name="password" placeholder="Passwort">
			<p>
				<input type="checkbox" name="checkbox_keep_loggedin">Angemeldet
				bleiben?
			</p>
			<div class="button-group">
				<button type="submit" name="login" value="login">Login</button>
				<button type="submit" name="register" value="register"
					formaction="register.jsp">Registrieren</button>
				
			</div>
			<p id="warning"></p>


		</form>
		</c:otherwise>
	</c:choose>

</body>
</html>