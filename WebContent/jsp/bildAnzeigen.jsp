<!--  Ersteller: Timo Iwan -->

<%@page import="beans.BildBean"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<%@ taglib prefix="c" 
	uri="http://java.sun.com/jsp/jstl/core"%>
	

<!DOCTYPE html>
<html>
	
			<head>
		 <meta charset="UTF-8">
		<title>SWC Wolnzach</title>
		<link href="../css/css_hauptseite.css" rel="stylesheet">
				</head>
				 <body>
				 	<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>
	<c:otherwise>
	<header>
		<%@ include file="../jspf/Header.jspf" %> 
	</header>

	 
	 
	 <div class="headers"><h2>hochgeladene Erinnerungen</h2></div>
	<div class="abstand">
		<div>
		<c:forEach var="bb" items="${bilder}" varStatus="status">
				<form id="Form" method="get" action="../BildServletListe">
				<img src="../BildServletGet?id=${bb.id}" alt="bild">${bb.info}
				</form>
		</c:forEach>		
	  </div>
	  </div>


	<footer>
		<%@ include file="../jspf/Footer.jspf"%>
	</footer>
	</c:otherwise>
	</c:choose>	
</body>			
</html>