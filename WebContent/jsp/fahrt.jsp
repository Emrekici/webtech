<!-- Ersteller: Emre Ekici -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>

<!DOCTYPE html>
<html lang="de">
<head>
<meta charset="UTF-8">
<title>SWC Wolnzach</title>
<base href="${pageContext.request.requestURI}" />
<link href="../css/css_hauptseite.css" rel="stylesheet">
<link rel=stylsheet type="text/css" href="<%=request.getContextPath() %>../css/css_hauptseite.css">
<script type="text/javascript" src="../js/BuchungScript.js"></script>
<script type="text/javascript" src="../js/term.js"></script>
</head>

<body>
				 	<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>
	<c:otherwise>
	<header>
		<%@ include file="../jspf/Header.jspf"%>
	</header>
	<div class="abstand">
	<div id="term">
		<button class="button" type="button"
			onclick="loadDoc('../txt/Teilnahme.txt', myFunction)">Teilnahmebedingungen anzeigen</button>
	
	</div>
	<form id="myForm" method="post" action="../jsp/Buchungsformular.jsp">
		<br>
		<b>Name: </b>${fahrt.name} <br>
		<b>Kategorie: </b>${fahrt.kategorie} <br>
		<b>Zielgebiet: </b>${fahrt.zielgebiet} <br>
		<b>Datum: </b>${fahrt.datum} <br>
		<b>Freie Plätze: </b>${fahrt.seats} <br>
		<button id="date" value="${fahrt.datum}"></button>
		<button type="submit" id="seats" value="${fahrt.seats}">Jetzt buchen</button>
	</form>
	<c:choose>
		<c:when test="${userAccount.role != 0}">
			<c:forEach var="teili" items="${teilnehmer}" varStatus="status">
				
					<b>${teili.key.vorname} ${teili.key.nachname}:    Anzahl Teilnehmer: ${teili.value}</b><br>				
			</c:forEach>
		</c:when>
	</c:choose>
	</div>
	<footer>
		<%@ include file="../jspf/Footer.jspf"%>
	</footer>
		</c:otherwise>
	</c:choose>	
</body>
</html>



