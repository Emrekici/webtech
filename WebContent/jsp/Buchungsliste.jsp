﻿<!-- Ersteller: Emre Ekici -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<title>Buchungsübersicht</title>
</head>
<body>
				 	<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>
	<c:otherwise>
	<header>
		<%@ include file="../jspf/Header.jspf"%>
	</header>

	<h1>
		<b> Profileinstellungen </b>
	</h1>

	<a href="../jsp/neueFahrtForm.jsp">Fahrt erstellen</a>

	<form id="FormularStyle" method="post" action="../BildBlobServlet"
		accept-charset="utf-8" enctype="multipart/form-data">
		<section>
			<h2>Erinnerungsbild hochladen</h2>
			<div>
				<label for="info">Infotext:</label>
				<textarea id="info" name="info" cols="35" rows="4" required></textarea>
				<label for="bild">Bild:</label> <input id="bild" name="bild"
					type="file" accept="image/*" required>
				<button type="submit">Erinnerung hochladen</button>

			</div>
		</section>
	</form>
	<h2>Buchungsübersicht</h2>
	<h3>Ihre Buchungen</h3>
	<c:forEach var="buchung" items="${buchungen}" varStatus="status">
		<form id="myForm" method="get" action="../BuchungLöschen">
			<br>
			<input value="${buchung.id}" name="id" type="hidden"></input> <br>
			<b>Vorname: </b>${buchung.vorname} <br>
			<b>Nachname: </b>${buchung.nachname} <br>
			<b>Zielgebiet: </b>${buchung.zielgebiet} <br>
			<b>Datum: </b>${buchung.datum} <br>
			<b>Tageskarte: </b>${buchung.tageskarte} <br>
			<b>Ausrüsstung: </b>${buchung.ausruesstung} <br>
			<b>Verreinsbus: </b>${buchung.verreinsbus} <br>
			<b>Einstiegsort: </b>${buchung.einstiegsort} <br>
			<b>Ihre Anmerkungen: </b>${buchung.anmerkung} <br>
			<button type="submit" value="löschen">Buchung stornieren</button>
		</form>
	</c:forEach>
	<footer>
		<%@ include file="../jspf/Footer.jspf"%>
	</footer>
		</c:otherwise>
	</c:choose>	
</body>
</html>


