<!-- Ersteller: Timo Iwan -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<base href="${pageContext.request.requestURI}" />
<meta charset="UTF-8">
<title>SWC Wolnzach</title>
<link href="../css/css_hauptseite.css" rel="stylesheet">
<link href="../css/Formularstyle.css" rel="stylesheet">
</head>
<header>
	<%@ include file="../jspf/Header.jspf"%>
</header>
<body>

<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>  
	<c:when test="${userAccount.role != 0}">
	
		<div class="headers">
		<h2>Neuigkeiten</h2>
	</div>
	<c:forEach var="news" items="${newsListe}" varStatus="status">
		<b>${news.newsText}</b><br>		
	</c:forEach>
	<div class="abstand"></div>
	
	<form id="FormularStyle" method="post" action="../NewsServlet"
		accept-charset="utf-8">
			<textarea id="newsText" name="newsText" cols="35" rows="4" required></textarea>
			<br>
			<button type="submit">Neuigkeit erstellen</button> 
	</form>      
        </c:when>
        <c:otherwise>
        	<div class="headers">
		<h2>Neuigkeiten</h2>
	</div>
	<c:forEach var="news" items="${newsListe}" varStatus="status">
		<b>${news.newsText}</b><br>		
	</c:forEach>
	<div class="abstand"></div>
	</c:otherwise>
</c:choose>
	<footer>
		<%@ include file="../jspf/Footer.jspf"%>
	</footer>
</body>
</html>
