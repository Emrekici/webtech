<!-- Ersteller: Emre Ekici -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="de">
<head>
<title>Buchungsformular</title>
<meta charset="utf-8">
<link rel="stylesheet" href="../css/FormularStyle.css">
<script src="../js/FormularScript.js"></script>
</head>
<body>
		<c:choose>
	<c:when test="${userAccount == null && cookie.token == null}">
		<c:redirect
			url="login.jsp?login=noLog" />
	</c:when>
	<c:otherwise>
	<header>
		<%@ include file="../jspf/Header.jspf" %> 
	</header>
	<section>
		<h2>Buchungsformular</h2>
		<form id="myForm" method="post" action="../Buchungsservlet" accept-charset="utf-8">
			<div>
				<label for="vorname">Vorname:</label>
				<input id="vorname" name="vorname" type="text" maxlength="40" size="30" value="${userAccount.vorname}" required>
			</div>
			<div>
				<label for="nachname">Nachname:</label>
				<input id="nachname" name="nachname" type="text" maxlength="40" size="30" value="${userAccount.nachname}" required>
			</div>
			<div>
				<label for="zielgebiet">Zielgebiet:</label> 
				<input id="zielgebiet" name="zielgebiet" type="text" value="${fahrt.zielgebiet}" required>
			</div>
			<div>
				<label for="datum">Datum:</label>
				<input type="date" id="datum" name="datum" value="${fahrt.datum}" readonly required>
			</div>
			<div>
				<label>Tageskarte auswählen:</label>
				<input type="radio" id="er" name="tageskarte" value="Erwachsen" checked> 					
				<label for="er"> Erwachsen</label>
				<input type="radio" id="ju" name="tageskarte" value="Jugendlich">
				<label for="ju">Jugendlich</label>
				<input type="radio" id="ki" name="tageskarte" value="Kind"> 
				<label for="ki">Kind</label>
			</div>
			<div>
				<label>Vereinsbusplatz reservieren:</label>
				<input type="radio" id="ja" name="vereinsbus" value="reservieren" checked> 					
				<label for="ja">Ja</label>
				<input type="radio" id="nein" name="vereinsbus" value="eigenfahrt">
				<label for="nein">Nein</label><br>
			</div>
			<div>
				<label for="einstiegsortBox" id="einstiegsort">Einstiegsort:</label>
				<select name="einstiegsort" id="einstiegsortBox">
					<option value="wolnzach" selected>Wolnzach</option>
					<option value="gosseltshausen">Gosseltshausen</option>
					<option value="burgstall">Burgstall</option>
				</select>
			</div>
			<div>
				<label>Ausrüsstung ausleihen:</label>
				<input type="radio" name="ausruesstung" value="nötig" checked> 					
				<label for="ja">Ja</label>
				<input type="radio" name="ausruesstung" value="vorhanden">
				<label for="nein">Nein</label><br>
			</div>
			<div>
				<label for="anmerkung">Anmerkungen:</label>
				<textarea name="anmerkung" id="anmerkung" placeholder="Hier Anmerkung eingeben..." rows="4" maxlength="300"></textarea>
			</div>
			<div>
				<button type="submit" name="absenden" value="absenden" >Absenden</button>
				<button type="reset" name="zuruecksetzen" value="zuruecksetzen" >Zurücksetzen</button>
			</div>
		</form>
	</section>
	<footer>
		<%@ include file="../jspf/Footer.jspf" %> 
	</footer>
		</c:otherwise>
	</c:choose>	
</body>
</html>