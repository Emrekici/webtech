<!-- Ersteller: Timo Iwan -->

<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

<head>

<meta charset="UTF-8">
<title>SWC Wolnzach</title>
<script type="text/javascript" src="../js/login.js"></script>
<link href="../css/css_hauptseite.css" rel="stylesheet">
<jsp:useBean id="now" class="java.util.Date" />
</head>
	<header>
		<%@ include file="../jspf/Header.jspf" %> 
	</header>
<body>
	<c:choose>

		<c:when test="${userAccount == null && cookie.token == null}">
			<c:redirect url="login.jsp?login=noLog" />

		</c:when>
		<c:otherwise>
			<div class="headers">
				<h1>
					<b> kommende Touren </b>
				</h1>
			</div>




			<div class="tabbed">
				<input checked="checked" id="tab1" type="radio" name="tabs" /> <input
					id="tab2" type="radio" name="tabs" /> <input id="tab3"
					type="radio" name="tabs" />

				<nav>
					<label for="tab1">Alle Touren</label> <label for="tab2">Wanderungen</label>
					<label for="tab3">Skifahrten</label>
				</nav>

				<figure>
					<div class="tab1">
						<div class="fahrtlinks">

							<c:forEach var="fahrti" items="${fahrten}" varStatus="status">
								<form id="Form" method="get" action="../FahrtServlet">




									<c:if test="${fahrti.datum gt now}">
										<c:choose>

											<c:when test="${fahrti.kategorie  eq 'wanderung'}">
												<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img src="../img/wanderungImage.png"
										alt="bild"></a>
												<div class="centered">
													<div class="box">${fahrti.name}<br>${fahrti.aDatum}</div>
												</div>
											</c:when>
											<c:otherwise>
												<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img src="../img/skiImage.png"
										alt="bild"></a>
												<div class="centered">
													<div class="box">${fahrti.name}<br>${fahrti.aDatum}</div>
												</div>
											</c:otherwise>
										</c:choose>


									</c:if>
								</form>
							</c:forEach>

						</div>
					</div>
					<div class="tab2">
						<div class="fahrtlinks">

							<c:forEach var="fahrti" items="${fahrten}" varStatus="status">
								<form id="Form2" method="get" action="../FahrtServlet">




									<c:if
										test="${fahrti.datum gt now and fahrti.kategorie eq 'wanderung'}">
										<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img
											src="../img/wanderungImage.png" alt="bild"></a>
										<div class="centered">
											<div class="box">${fahrti.name}<br>${fahrti.aDatum}</div>
										</div>


									</c:if>
								</form>
							</c:forEach>

						</div>
					</div>

					<div class="tab3">
						<div class="fahrtlinks">

							<c:forEach var="fahrti" items="${fahrten}" varStatus="status">
								<form id="Form3" method="get" action="../FahrtServlet">




									<c:if
										test="${fahrti.datum gt now and fahrti.kategorie eq 'skifahrt'}">
										<a href="../FahrtInfoServlet?fahrt_id=${fahrti.id}"> <img
											src="../img/skiImage.png" alt="bild"></a>
										<div class="centered">
											<div class="box">${fahrti.name}<br>${fahrti.aDatum}</div>
										</div>


									</c:if>
								</form>
							</c:forEach>

						</div>
					</div>
				</figure>
			</div>
		</c:otherwise>
	</c:choose>
</body>
<footer>
	<hr>
	<a href="../jsp/hauptseite.jsp">Zurück zur Hauptseite</a>
</footer>
</html>