<!-- Ersteller: Patrick Eibel -->

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="de">
<head>
    <title>tryLogin</title>
    <base href="${pageContext.request.requestURI}" />
</head>
<body>
<h1>Index</h1>
<main>
    <jsp:forward page="/CheckTokenServlet"/>
</main>
</body>
</html>