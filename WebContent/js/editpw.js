// Ersteller: Patrick Eibel

"use strict";

document.addEventListener("DOMContentLoaded", init);

function init() {
    checkInformation();
}

function checkInformation() {
    const params = new URLSearchParams(window.location.search);
    if (params.has('status')) {
        var status = params.get('status');
        if (status === 'nomatch') {
            var notFound = "Die Eingebeben Werte stimmen nicht überein";
            document.getElementById('warning').innerHTML = notFound;
        }
        else if (status === 'success')
            var notFound = "Das Passwort wurde erfolgreich geändert.";
        document.getElementById('warning').innerHTML = notFound;
    }
}