// Ersteller: Emre Ekici

document.addEventListener("DOMContentLoaded", init);

function init() {
	var seatsElement = document.getElementById("seats");
	var dateElement = document.getElementById("date");
	var time = new Date().getTime(); // get your number
	var date = new Date(time); // create Date object
	date = (date.getYear()+1900) + "-" + getMonth(date.getMonth()) + "-" + getDay(date.getDate());
	if (seatsElement.value < 1 || dateElement.value <= date) {
	document.getElementById("seats").style.visibility = "hidden";
	}
}

function getMonth(month) {
    return (month < 10 ? '0' : '') + month;
}

function getDay(day) {
    return (day < 10 ? '0' : '') + day;
}