// Ersteller: Patrick Eibel

"use strict";

document.addEventListener("DOMContentLoaded", init);

function init(){
    checkInformation();
}

function checkInformation() {
    const params = new URLSearchParams(window.location.search);
    if(params.has('login')){
        var login = params.get('login');
        if(login === 'false'){
            var notFound = "Email oder Passwort falsch! Bitte erneut versuchen";
            document.getElementById('warning').innerHTML = notFound;
        }
        else if(login === 'noLog'){
            var notFound = "Bitte anmelden für Zugriff auf diese Seite";
            document.getElementById('warning').innerHTML = notFound;
        }
        else if(login === 'register'){
            var notFound = "Erfolgreich registriert bitte anmelden";
            document.getElementById('warning').innerHTML = notFound;
        }
    }
}