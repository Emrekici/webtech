// Ersteller: Emre Ekici

document.addEventListener("DOMContentLoaded", init);

function init() {
	var element = document.getElementById("ja");
	element.addEventListener("change", eingabefeld);
	element = document.getElementById("nein");
	element.addEventListener("change", eingabefeld);
}

function eingabefeld() {
	document.getElementById("einstiegsort").hidden = document.getElementById("nein").checked;
	document.getElementById("einstiegsortBox").hidden = document.getElementById("nein").checked;
}
