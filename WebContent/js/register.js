// Ersteller: Patrick Eibel

"use strict";

document.addEventListener("DOMContentLoaded", init);

function init(){
    register();
}

function register() {
    const params = new URLSearchParams(window.location.search);
    if(params.has('register')){
        var register = params.get('register');
        if(register === 'false'){
            var notFound = "Die Registierung war nicht erfolgreich - Email bereits vergeben";
            document.getElementById('warning').innerHTML = notFound;
        }
       }
}