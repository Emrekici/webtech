CREATE TABLE news (
    id INT  NOT NULL auto_increment,
    newsText VARCHAR(255) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE buchungen (
	id int auto_increment,
	
	user_id int,
	fahrt_id int,
	vorname varchar(50),
	nachname varchar(50),
	zielgebiet varchar(50),
	datum date,
	tageskarte varchar(50),
	ausruesstung varchar(50),
	verreinsbus varchar(50),
	einstiegsort varchar(50),
	anmerkung varchar(50),
	
	PRIMARY KEY (id)
);

CREATE TABLE fahrten (
	id int auto_increment,
	
	name varchar(30),
	kategorie varchar(30),
	zielgebiet varchar(30),
	datum varchar(30),
	seats int,
	
	PRIMARY KEY (id)
);

CREATE TABLE bilder (
    id INT NOT NULL auto_increment,
    info VARCHAR(255) NOT NULL,
    bild BLOB NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE users (
	User_ID int auto_increment not null,
	
	email varchar(255) not null,
	password varchar(255) not null,
	Anrede varchar(4) not null,
	Vorname varchar(25) not null,
	Nachname varchar(25) not null,
	Geburtsdatum date default null,
	Vereinsmitglied varchar(5) not null,
	Role int(11) default null,
	token varchar(4096) default null,
	
	PRIMARY KEY (User_ID)
	) ENGINE=InnoDB default charset=utf8mb4;

